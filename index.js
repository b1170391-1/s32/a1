const express = require ("express");
const app = express();
const PORT = process.env.PORT || 4000;
const mongoose = require ("mongoose");

const courseRoutes = require ('./routes/courseRoutes');


app.use (express.json());
app.use(express.urlencoded({extended:true}));

app.use ("/api/courses", courseRoutes);

mongoose.connect('mongodb+srv://jlumactud:jellyanne@batch139.poyy4.mongodb.net/course-booking?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log (`Connected to database`)
);



app.listen(PORT, () => console.log (`Server running at port ${PORT}`));